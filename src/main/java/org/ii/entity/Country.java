package org.ii.entity;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="country")
@XmlType(propOrder={"counties","stores","name","id"})
@Table(name = "COUNTRY")
@Entity
public class Country implements Comparable<Country> {
	private static final long serialVersionUID = 1L;
	
	protected Country(){
	}	
	
	public Country(String id, String name){
		this.id = id;
		this.name = name;
	}
	/*Unique id is in iso-3166-1 format for country code*/
	@Id
	@Column(name="ID")
	@XmlAttribute(name="id")
	private String id;

	@XmlAttribute(name="name")
	private String name;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = County.class)
	@XmlElementWrapper(name = "counties")
	@XmlElement(name = "county")
	private Set<County> counties;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Store.class)
	@XmlElementWrapper(name = "stores")
	@XmlElement(name = "store")
	private TreeSet<Store> stores;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setCounties(Set<County> counties) {
		this.counties = counties;
	}
	public Set<County> getCounties() {
		return counties;
	}

	public TreeSet<Store> getStores() {
		return stores;
	}
	public void setStores(TreeSet<Store> stores) {
		this.stores = stores;
	}
	public int compareTo(Country aCountry) {
		return getName().compareToIgnoreCase(aCountry.getName());
	}

}
