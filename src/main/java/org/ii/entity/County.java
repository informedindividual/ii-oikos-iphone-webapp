package org.ii.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"stores","name","id"})
@Entity
@Table(name = "COUNTY")
public class County implements Serializable, Comparable<County> {
	private static final long serialVersionUID = 1L;

	public County(){
		
	}
	
	public County(String id, String name){
		this.id = id;
		this.name = name;
	}
	
	/*Unique id is in ISO-3166-2 format for country subdivision code*/
	@Id
	@Column(name="ID")
	@XmlAttribute(name = "id")
	private String id;
	
	@XmlAttribute(name = "name")
	@Column(name = "NAME")
	private String name;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Store.class)
	@XmlElementWrapper(name = "stores")
	@XmlElement(name = "store")
	private Set<Store> stores;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(County aCounty) {
		return getName().compareToIgnoreCase(aCounty.getName());
	}

	public Set<Store> getStores() {
		return stores;
	}

	public void setStores(Set<Store> stores) {
		this.stores = stores;
	}
	
}
