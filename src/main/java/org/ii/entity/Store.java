package org.ii.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"places","logo","logoUrl","name","id"})
@Entity
@Table(name = "STORE")
public class Store implements Serializable, Comparable<Store> {
	private static final long serialVersionUID = 1L;
	
	public Store(){
	}
	public Store(String name, String logo){
		this.name=name;
		this.logo=logo;
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue
	@XmlAttribute(name = "id")
	private Integer id;
	
	@Column(name = "NAME")
	@XmlAttribute(name = "name")
	private String name;
	
	@XmlAttribute(name = "logoUrl")
	private static final String logoUrl = "http://ii42.org/oikos/img/store/";
	
	@Column(name = "LOGO")
	@XmlAttribute(name = "logo")
	private String logo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Place.class)
	@XmlElementWrapper(name = "places")
	@XmlElement(name = "place")
	private List<Place> places;
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogoUrl() {
		return logoUrl;
	}
	
	public List<Place> getPlaces() {
		return places;
	}
	public void setPlaces(List<Place> places) {
		this.places = places;
	}
	
	public int compareTo(Store aStore) {
		return getName().compareTo(aStore.getName());
	}
	
	
}
