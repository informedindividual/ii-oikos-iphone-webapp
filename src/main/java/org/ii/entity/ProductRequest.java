package org.ii.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT_REQUEST")
public class ProductRequest implements Serializable, Comparable<ProductRequest>{
	private static final long serialVersionUID = 1L;

	public ProductRequest(){}
	
	public ProductRequest(String productName, String productTypeId, String productType, String storeId, String store, String countyId, String county, String placeId, String place, String device){
		this.productName=productName;
		this.productTypeId=productTypeId;
		this.productType=productType;
		this.storeId=storeId;
		this.store=store;
		this.countyId=countyId;
		this.county=county;
		this.placeId=placeId;
		this.place=place;
		this.device=device;
		this.date=new Date();
	}
	
	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer id;
	
	@Column(name = "PRODUCTNAME")
	private String productName;
	
	@Column(name = "PRODUCT_TYPE_ID")
	private String productTypeId;
	
	@Column(name = "PRODUCT_TYPE")
	private String productType;
	
	@Column(name = "STOREID")
	private String storeId;
	
	@Column(name = "STORE")
	private String store;
	
	@Column(name = "COUNTYID")
	private String countyId;
	
	@Column(name = "COUNTY")
	private String county;
	
	@Column(name = "PLACEID")
	private String placeId;
	
	@Column(name = "PLACE")
	private String place;

	@Column(name = "DEVICE")
	private String device;
	
	@Column(name = "DATE")
	private Date date;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(String productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getCountyId() {
		return countyId;
	}

	public void setCountyId(String countyId) {
		this.countyId = countyId;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
	
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int compareTo(ProductRequest aProductRequest) {
		return getProductName().compareTo(aProductRequest.getProductName());
	}	

}
