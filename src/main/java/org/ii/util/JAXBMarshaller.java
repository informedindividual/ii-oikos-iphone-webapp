package org.ii.util;

import java.io.File;
import java.io.IOException;

import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Service;

@Service
@Named
public class JAXBMarshaller {

	public void marshallXMLToFile(Object o, String filename)
			throws JAXBException {

		JAXBContext context = JAXBContext.newInstance(o.getClass());
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(o, new File(filename));
		System.out.println("XML file written to " + filename);
	}
	
	public void marshallXMLToOutputStream(Object object, HttpServletResponse res)
	throws JAXBException, IOException {

		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/xml; charset=UTF-8");
		
		JAXBContext context = JAXBContext.newInstance( object.getClass());
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		m.marshal(object, res.getOutputStream());
	}
	
}
