package org.ii.service;

import java.util.List;
import java.util.Set;

import org.ii.entity.Place;

public interface PlaceService {

	public abstract void add(Place county);
	public abstract void delete(Place county);
	public abstract void edit(Place county);
	public abstract List<Place> getAll();
}