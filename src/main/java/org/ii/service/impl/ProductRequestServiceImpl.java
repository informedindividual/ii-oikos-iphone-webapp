package org.ii.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.ii.entity.ProductRequest;
import org.ii.service.ProductRequestService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Named
public class ProductRequestServiceImpl implements ProductRequestService{
	protected static Logger logger = Logger.getLogger("service");
	
	@Inject
	private SessionFactory sessionFactory;
	
	/**
	 * Retrieves all ProductRequest's
	 * @return a Set of ProductRequest
	 */
	public List<ProductRequest> getAll() {
		logger.debug("Retrieving all product requests");
		
		final Session session = sessionFactory.getCurrentSession();

		return new ArrayList<ProductRequest>(session.createQuery("FROM ProductRequest").list());
	}
	
	/**
	 * Adds a new product request
	 * */
	public void add(ProductRequest productRequest){
		logger.debug("Adding new productRequest");
		
		Session session = sessionFactory.getCurrentSession();
		session.save(productRequest);
	}
}
