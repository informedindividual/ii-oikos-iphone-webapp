package org.ii.service.impl;


import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.ii.entity.County;
import org.ii.service.CountyService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Named
public class CountyServiceImpl implements CountyService {
	protected static Logger logger = Logger.getLogger("service");
	
	@Inject
	private SessionFactory sessionFactory;

	/**
	 * Retrieves all counties
	 * @return a Set of counties
	 */
	public Set<County> getAll() {
		logger.debug("Retrieving all counties");
		
		final Session session = sessionFactory.getCurrentSession();

		return new HashSet<County>(session.createQuery("FROM County").list());
	}
	
	/**
	 * Retrieves a county
	 * @iso_3166_2
	 * @return a County
	 */
	public County get(String id) {
		logger.debug("Retrieving county by id");
		
		final Session session = sessionFactory.getCurrentSession();

		// Retrieve existing person first
		return (County) session.get(County.class, id);
	}	
	
	

	/**
	 * Adds a new county
	 * */
	public void add(County county){
		logger.debug("Adding new county");
		
		Session session = sessionFactory.getCurrentSession();
		session.save(county);
	}
	
	/**
	 * Deletes an existing county
	 * @param ISO_3166_2 the ISO_3166_2-id of the existing county
	 */
	public void delete(County county) {
		logger.debug("Deleting existing county");
		
		// Retrieve session from Hibernate
		Session session = sessionFactory.getCurrentSession();
		
		// Delete 
		session.delete(county);
	}
	/**
	 * Edits an existing county
	 */
	public void edit(County county) {
		logger.debug("Editing existing county");
		
		// Retrieve session from Hibernate
		Session session = sessionFactory.getCurrentSession();

		// Save updates
		session.save(county);
	}
}

