package org.ii.service.impl;


import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.ii.entity.Store;
import org.ii.service.StoreService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Named
public class StoreServiceImpl implements StoreService {
	protected static Logger logger = Logger.getLogger("service");
	
	@Inject
	private SessionFactory sessionFactory;

	/**
	 * Retrieves all stores
	 * @return a Set of stores
	 */
	public Set<Store> getAll() {
		logger.debug("Retrieving all stores");
		
		final Session session = sessionFactory.getCurrentSession();

		return new HashSet<Store>(session.createQuery("FROM Store").list());
	}
	
	/**
	 * Retrieves a store
	 * @iso_3166_2
	 * @return a store
	 */
	public Store get(Integer id) {
		logger.debug("Retrieving store by id");
		
		final Session session = sessionFactory.getCurrentSession();

		// Retrieve existing store first
		return (Store) session.get(Store.class, id);
	}	
	
	

	/**
	 * Adds a new store
	 * */
	public void add(Store store){
		logger.debug("Adding new store");
		
		Session session = sessionFactory.getCurrentSession();
		session.save(store);
	}
	
	/**
	 * Deletes an existing store
	 * @param id the id of the existing store
	 */
	public void delete(Store store) {
		logger.debug("Deleting existing store");
		
		// Retrieve session from Hibernate
		Session session = sessionFactory.getCurrentSession();
		
		// Delete 
		session.delete(store);
	}
	/**
	 * Edits an existing store
	 */
	public void edit(Store store) {
		logger.debug("Editing existing store");
		
		// Retrieve session from Hibernate
		Session session = sessionFactory.getCurrentSession();
		
		session.save(store);
	}
}

