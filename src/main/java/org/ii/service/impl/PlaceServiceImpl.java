package org.ii.service.impl;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.ii.entity.County;
import org.ii.entity.Place;
import org.ii.service.CountyService;
import org.ii.service.PlaceService;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Named
public class PlaceServiceImpl implements PlaceService {
	protected static Logger logger = Logger.getLogger("service");
	
	@Inject
	private SessionFactory sessionFactory;


	public List<Place> getAll() {
		logger.debug("Retrieving all places");
		
		final Session session = sessionFactory.getCurrentSession();

		return new ArrayList<Place>(session.createQuery("FROM Place").list());
	}

	public void add(Place place){
		logger.debug("Adding new place");
		
		Session session = sessionFactory.getCurrentSession();
		session.save(place);
	}
	

	public void delete(Place place) {
		logger.debug("Deleting existing place");
		
		// Retrieve session from Hibernate
		Session session = sessionFactory.getCurrentSession();
		
		// Delete 
		session.delete(place);
	}
	/**
	 * Edits an existing place
	 */
	public void edit(Place place) {
		logger.debug("Editing existing place");

		Session session = sessionFactory.getCurrentSession();
		
		// Save updates
		session.save(place);
	}
}

