package org.ii.service;

import java.util.Set;

import org.ii.entity.County;

public interface CountyService {

	public abstract void add(County county);
	public abstract void delete(County county);
	public abstract void edit(County county);
	public abstract County get(String id);
	public abstract Set<County> getAll();
}