package org.ii.service;

import java.util.List;

import org.ii.entity.ProductRequest;

public interface ProductRequestService {
	public abstract void add(ProductRequest productRequest);
	public abstract List<ProductRequest> getAll();
}
