package org.ii.service;

import java.util.Set;

import org.ii.entity.Store;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("personService")
@Transactional
public interface StoreService {

	public abstract void add(Store store);
	public abstract void delete(Store store);
	public abstract void edit(Store store);
	public abstract Store get(Integer id);
	public abstract Set<Store> getAll();
}