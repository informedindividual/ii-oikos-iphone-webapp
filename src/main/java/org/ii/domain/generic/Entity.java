package org.ii.domain.generic;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;

@javax.persistence.MappedSuperclass
public abstract class Entity {

	public Entity() {
		this.uuid = UUID.randomUUID().toString();
	}

	@Id
	@Column(name = "ID")
	private final String uuid;

	@Override
	public int hashCode() {
		return uuid.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Entity)) {
			return false;
		}
		final Entity other = (Entity) obj;
		return getUuid().equals(other.getUuid());
	}

	public String getUuid() {
		return uuid;
	}
}
