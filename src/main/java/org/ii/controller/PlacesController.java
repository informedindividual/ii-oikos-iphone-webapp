package org.ii.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.ii.entity.Country;
import org.ii.entity.County;
import org.ii.entity.Place;
import org.ii.entity.Store;
import org.ii.service.CountyService;
import org.ii.service.PlaceService;
import org.ii.service.StoreService;
import org.ii.util.JAXBMarshaller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/places")
public class PlacesController {

	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	private CountyService countyService;
	
	@Inject
	private PlaceService placeService;
	
	@Inject
	private StoreService storeService;
	
	@Inject 
	JAXBMarshaller jaxbMarshaller;
	
    @RequestMapping(value="/getAll/xml", method={RequestMethod.GET, RequestMethod.POST})
	public void getPlaces(HttpServletRequest req, HttpServletResponse res,
    		@RequestParam(value="countryId", required=false) String countryId,
    		@RequestParam(value="countyId", required=true) String countyId,
    		@RequestParam(value="storeId", required=true) String storeId) 		
    		throws IOException, JAXBException
    	{
		
    	logger.debug("Received request to get all stores in county");		

    	//For now default to country NO 
    	Country country = new Country("NO", "Norge");
		County county = countyService.get(countyId);
		
		//@TODO get only places filtered by store/county
		List<Place> places = placeService.getAll();
		
		Store store = storeService.get(new Integer(storeId));
		store.setPlaces(places);
		
		Set<Store> stores = new TreeSet<Store>();		
		stores.add(store);
		
		county.setStores(stores);
		
		Set<County> counties = new HashSet<County>();
		counties.add(county);
		
		country.setCounties(counties);
		jaxbMarshaller.marshallXMLToOutputStream(country, res);

	  }	
	
}
