package org.ii.controller;

import java.io.IOException;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.ii.entity.Country;
import org.ii.entity.Store;
import org.ii.service.StoreService;
import org.ii.util.JAXBMarshaller;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/stores")
public class StoreController {
	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	private StoreService storeService;
	
	@Inject
	private JAXBMarshaller jaxbMarshaller;
	
	/**
	 * Handles and retrieves all stores and show it in a JSP page
	 * 
	 * @return the name of the JSP page
	 */
    @RequestMapping(value = "getAll", method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView getStores(
    		@RequestParam(value="countryId", required=false) String countryId) {
    	logger.debug("Received request to show all stores");
    	
    	TreeSet<Store> stores = new TreeSet<Store>(storeService.getAll());
    	
    	ModelAndView modelAndView = new ModelAndView("store/getAll");
    	modelAndView.addObject("stores", stores);
    	return modelAndView;
	}

    @RequestMapping(value = "getAll/xml", method = {RequestMethod.GET,RequestMethod.POST})
    public void getStoresXml(HttpServletResponse res,
    		@RequestParam(value="countryId", required=false) String countryId,
    		@RequestParam(value="countyId", required=false) String countyId,
    		@RequestParam(value="storeId", required=false) String storeId) 
    	throws IOException, JAXBException
    {
    	logger.debug("Received request to show all stores in xml");
    	
    	Country guessedCountry = new Country("NO","Norge");
    	TreeSet<Store> stores = new TreeSet<Store>(storeService.getAll());
    	guessedCountry.setStores(stores);
    	  
    	jaxbMarshaller.marshallXMLToOutputStream(guessedCountry, res);
	}
    
    /**
     * Adds a new store by delegating the processing to StoreService.
     * Displays a confirmation JSP page
     * 
     * @return  the name of the JSP page
     */
    @Transactional
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add(
    		@RequestParam(value="name", required=true) String name,
    		@RequestParam(value="logo", required=false) String logo) {
   
		logger.debug("Received request to add new person");
		
		Store store = new Store(name, logo);
		storeService.add(store);
		
		ModelAndView modelAndView = new ModelAndView("store/added");
		modelAndView.addObject("store", store);

		return modelAndView;
	}
    
    
}
