package org.ii.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.ii.entity.County;
import org.ii.entity.Place;
import org.ii.entity.Store;
import org.ii.service.CountyService;
import org.ii.service.PlaceService;
import org.ii.service.StoreService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IphoneController  {
	
	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	CountyService countyService;	
	
	@Inject
	StoreService storeService;
	
	@Inject
	PlaceService placeService;
	
	@RequestMapping(value = "/iphone", method = RequestMethod.GET)
	protected ModelAndView getIPhoneView() {
		logger.debug("Received request to getIPhoneView");
		
		TreeSet<County> counties = new TreeSet<County>(countyService.getAll());
		TreeSet<Store> stores = new TreeSet<Store>(storeService.getAll());
		List<Place> places = new ArrayList<Place>(placeService.getAll());
		
		ModelAndView modelAndView = new ModelAndView("mobile/iphone");
		modelAndView.addObject("counties", counties);
		modelAndView.addObject("stores", stores);
		modelAndView.addObject("places", places);

		return modelAndView;
	}
}
