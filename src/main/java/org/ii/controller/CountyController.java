package org.ii.controller;

import java.io.IOException;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.ii.entity.Country;
import org.ii.entity.County;
import org.ii.service.CountyService;
import org.ii.util.JAXBMarshaller;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


/**
 * Handles and retrieves counties request
 */
@Controller
@RequestMapping("/counties")
public class CountyController {

	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	private CountyService countyService;
	
	@Inject 
	private JAXBMarshaller jaxbMarshaller;
	
	/**
	 * Handles and retrieves all counties and show it in a JSP page
	 * 
	 * @return the name of the JSP page
	 */
    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public ModelAndView getCounties() {
    	logger.debug("Received request to show all counties");
    	
    	// Retrieve all counties by delegating the call to CountyService
    	TreeSet<County> counties = new TreeSet<County>(countyService.getAll());
    	
    	ModelAndView modelAndView = new ModelAndView("county/getAll");
    	modelAndView.addObject("counties", counties);
    	return modelAndView;
	}
    
    @RequestMapping(value = "getAll/xml", method = RequestMethod.GET)
    public void getXml(HttpServletResponse res) 
    	throws JAXBException, IOException {
    
    	logger.debug("Received request to show all counties in xml");
    	
    	Country guessedCountry = new Country("NO", "Norge");
    	
    	// Retrieve all counties by delegating the call to CountyService
    	TreeSet<County> counties = new TreeSet<County>(countyService.getAll());
		guessedCountry.setCounties(counties);
		
    	jaxbMarshaller.marshallXMLToOutputStream(guessedCountry, res);
	}    

    @Transactional
    @RequestMapping(value = "/add", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView add(
    		@RequestParam(value="iso_3166_2", required=true) String iso_3166_2,
    		@RequestParam(value="name", required=true) String name) {
		logger.debug("Received request to add new county");
		
    	County county = new County(iso_3166_2, name);
    	countyService.add(county);
    	
    	ModelAndView modelAndView = new ModelAndView("county/added");
    	modelAndView.addObject("county", county);
		
		return modelAndView;
	}	    
    
}
