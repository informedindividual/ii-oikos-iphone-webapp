package org.ii.controller.prodreq;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.CellStyle;
import org.ii.entity.ProductRequest;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class ProductRequestExcelView extends AbstractExcelView
{
	private short NUMBER_OF_SHEET = 6;
	
	@Override
	protected void buildExcelDocument(
			Map<String, Object> model, HSSFWorkbook workbook, 
			HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		 
		
		res.setContentType("application/vnd.ms-excel");
		res.setHeader("Content-Disposition", "attachment; filename=\"oikos-rapport.xls\"");

		List<ProductRequest> productRequests = (List<ProductRequest>) model.get("productRequests");
		
		HSSFCellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.
				getBuiltinFormat("m/d/yy h:mm"));

		//create a wordsheet
		HSSFSheet sheetAll = workbook.createSheet("Alle etterspørsler");
		HSSFSheet sheetCoop = workbook.createSheet("Coop");
		HSSFSheet sheetNG = workbook.createSheet("NG");
		HSSFSheet sheetIca = workbook.createSheet("Ica");
		HSSFSheet sheetRema = workbook.createSheet("Rema 1000");
		HSSFSheet sheetBunnpris = workbook.createSheet("Bunnpris");
		

		HSSFRow headerAll = sheetAll.createRow(0);
		HSSFRow headerCoop = sheetCoop.createRow(0);
		HSSFRow headerNG = sheetNG.createRow(0);
		HSSFRow headerIca = sheetIca.createRow(0);
		HSSFRow headerRema = sheetRema.createRow(0);
		HSSFRow headerBunnpris = sheetBunnpris.createRow(0);
		
		Vector<HSSFRow> storeHeaders = new Vector<HSSFRow>();
		storeHeaders.add(headerCoop);
		storeHeaders.add(headerNG);
		storeHeaders.add(headerIca);
		storeHeaders.add(headerRema);
		storeHeaders.add(headerBunnpris);
		
		headerAll.createCell(0).setCellValue("Etterspurt produkt");
		headerAll.createCell(1).setCellValue("Produkttype");
		headerAll.createCell(2).setCellValue("Kjede");
		headerAll.createCell(3).setCellValue("Fylke");
		headerAll.createCell(4).setCellValue("Sted");
		headerAll.createCell(5).setCellValue("Dato");

		Iterator<HSSFRow> it = storeHeaders.iterator();
		while (it.hasNext()){
			HSSFRow row = it.next();
			row.createCell(0).setCellValue("Kjede");
			row.createCell(1).setCellValue("Fylke");
			row.createCell(2).setCellValue("Sted");
			row.createCell(3).setCellValue("Produkttype");
			row.createCell(4).setCellValue("Etterspurt produkt");
			row.createCell(5).setCellValue("Dato");
		}
		
		int rowNum = 1;
		int coopRowNum = 1;
		int icaRowNum = 1;
		int remaRowNum = 1;
		int bunnprisRowNum = 1;
		int ngRowNum = 1;
		
		Iterator<ProductRequest> pIt = productRequests.iterator();
    	ProductRequest p;
    	while (pIt.hasNext()){
    		p = pIt.next();
    		HSSFRow row = sheetAll.createRow(rowNum++);
    		row.createCell(0).setCellValue(p.getProductName());
    		row.createCell(1).setCellValue(p.getProductType());
    		row.createCell(2).setCellValue(p.getStore());
    		row.createCell(3).setCellValue(p.getCounty());
    		row.createCell(4).setCellValue(p.getPlace());
    		if (p.getDate()!=null){
    			HSSFCell c = row.createCell(5);
    			c.setCellStyle(dateCellStyle);
    			c.setCellValue(p.getDate());        			
    		}
    		if (p.getStore()!=null){
	    		if (p.getStore().toLowerCase().contains("coop")){
	    			row = sheetCoop.createRow(coopRowNum++);
	    		}else if (p.getStore().toLowerCase().contains("ica")){
	    			row = sheetIca.createRow(icaRowNum++);
	    		}else if (p.getStore().toLowerCase().contains("rema")){
	    			row = sheetRema.createRow(remaRowNum++);
	    		}else if (p.getStore().toLowerCase().contains("bunnpris")){
	    			row = sheetBunnpris.createRow(bunnprisRowNum++);
	    		}else{
	    			row = sheetNG.createRow(ngRowNum++);
	    		}
	    		row.createCell(0).setCellValue(p.getStore());
    			row.createCell(1).setCellValue(p.getCounty());
    			row.createCell(2).setCellValue(p.getPlace());
    			row.createCell(3).setCellValue(p.getProductType());
    			row.createCell(4).setCellValue(p.getProductName());
        		if (p.getDate()!=null){
        			HSSFCell c = row.createCell(5);
        			c.setCellStyle(dateCellStyle);
        			c.setCellValue(p.getDate());        			
        		}
	    	}
    	}
    	
    	for (int sheetNum=0;sheetNum<NUMBER_OF_SHEET;sheetNum++){
    		HSSFSheet sheet = workbook.getSheetAt(sheetNum);
    		for (int columnNum=0;columnNum<sheet.getLastRowNum();columnNum++)
    			sheet.autoSizeColumn(columnNum);
    	}
	}
	
}
