package org.ii.controller.prodreq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.ii.entity.County;
import org.ii.entity.ProductRequest;
import org.ii.service.CountyService;
import org.ii.service.ProductRequestService;
import org.ii.service.StoreService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
@RequestMapping("/prodReq/getAll/excel")
public class ProductRequestExcelController extends AbstractController {

	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	ProductRequestService productRequestService;
	
	@Inject 
	CountyService countyService;
	
	@Inject 
	StoreService storeService;

	@Override
	@RequestMapping(value = "", method = {RequestMethod.GET, RequestMethod.POST})
	protected ModelAndView handleRequestInternal(HttpServletRequest req, 
			HttpServletResponse res) throws Exception {

		List<ProductRequest> productRequests = new ArrayList<ProductRequest>(productRequestService.getAll());
    	TreeSet<County> counties = new TreeSet<County>(countyService.getAll());
    	
    	Iterator<ProductRequest> pIt = productRequests.iterator();
    	ProductRequest p;
    	while (pIt.hasNext()){
    		p = pIt.next();
    		if (p.getStoreId()!=null && !"".equals(p.getStoreId()) && (p.getStore()==null || "".equals(p.getStore()))){
    			p.setStore(storeService.get(new Integer(p.getStoreId())).getName());
    		}
    		if (p.getCountyId()!=null && !p.getCountyId().equals("") && (p.getCounty()==null || "".equals(p.getStore()))){
    			p.setCounty(countyService.get(p.getCountyId()).getName());
    		}
    	}
		return new ModelAndView("ProductRequestExcelView","productRequests",productRequests);
	}
	
	
}
