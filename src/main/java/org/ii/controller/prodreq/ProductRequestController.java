package org.ii.controller.prodreq;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.ii.entity.County;
import org.ii.entity.ProductRequest;
import org.ii.entity.Store;
import org.ii.service.CountyService;
import org.ii.service.PlaceService;
import org.ii.service.ProductRequestService;
import org.ii.service.StoreService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
@RequestMapping("/prodReq")
public class ProductRequestController {
	
	protected static Logger logger = Logger.getLogger("controller");
	
	@Inject
	ProductRequestService productRequestService;
	
	@Inject 
	CountyService countyService;
	
	@Inject 
	StoreService storeService;
	
    @Transactional
    @RequestMapping(value = "/add", method = {RequestMethod.GET, RequestMethod.POST})
    public void add(
    		@RequestParam(value="productName", required=true) String productName,
    		@RequestParam(value="productTypeId", required=false) String productTypeId,
    		@RequestParam(value="productType", required=false) String productType,
    		@RequestParam(value="storeId", required=false) String storeId,
    		@RequestParam(value="store", required=false) String store,
    		@RequestParam(value="countyId", required=false) String countyId,
    		@RequestParam(value="county", required=false) String county,
    		@RequestParam(value="placeId", required=false) String placeId,
    		@RequestParam(value="place", required=false) String place,
    		@RequestParam(value="device", required=false) String device,
    		HttpServletResponse res) throws Exception {
		
    	logger.debug("Received request to add new product");
    	
		Assert.isTrue(!"".equals(productName), "productName must have a value");
		if (storeId==null || "".equals(storeId)){
			Assert.isTrue(store!=null && !"".equals(store), "Store or storeId must be selected");
		}
		
    	ProductRequest productRequest = new ProductRequest(productName, productTypeId, productType, storeId, store, countyId, county, placeId, place, device);
    	productRequestService.add(productRequest);
		
    	res.setCharacterEncoding("UTF-8");
		res.setContentType("text/xml; charset=UTF-8");
    	Writer out = res.getWriter();
    	out.write("Takk for at du gjør det enklere å velge økologisk");
	}	
    
    @RequestMapping(value = "/getAll", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView getAllProductRequests() {
    	logger.debug("Received request to show all product requests");

    	List<ProductRequest> productRequests = new ArrayList<ProductRequest>(productRequestService.getAll());
    	
    	TreeSet<County> counties = new TreeSet<County>(countyService.getAll());
    	
    	Iterator<ProductRequest> pIt = productRequests.iterator();
    	ProductRequest p;
    	while (pIt.hasNext()){
    		p = pIt.next();
    		if (p.getStoreId()!=null && !"".equals(p.getStoreId()) && (p.getStore()==null || "".equals(p.getStore()))){
    			p.setStore(storeService.get(new Integer(p.getStoreId())).getName());
    		}
    		if (p.getCountyId()!=null && !p.getCountyId().equals("") && (p.getCounty()==null || "".equals(p.getStore()))){
    			p.setCounty(countyService.get(p.getCountyId()).getName());
    		}
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("productRequest/getAll");
    	modelAndView.addObject("productRequests", productRequests);
    	
    	return modelAndView;
	}
    
}
