<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<h1>Stores</h1>

<p>You have added a new store at</p>
<%= new java.util.Date() %>

	<table>
		<tr>
			<td width="150">Id</td>
			<td width="150">Name</td>
		</tr>	
		<tr>
			<td><c:out value="${store.id}" /></td>
			<td><c:out value="${store.name}" /></td>
		</tr>
	</table>
</body>
</html>