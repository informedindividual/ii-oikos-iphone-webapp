<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<style>
		table td{
			border:1px solid black;
		}
	</style>
		
</head>
<body>

<table>
	<tr>
		<td width="25">Id</td>
		<td width="100">Name</td>
		<td width="350">LogoUrl</td>
		<td width="100">Logo</td>
		<td width="200">Result</td>
	</tr>
	<c:forEach items="${stores}" var="store">
		<tr>
			<td><c:out value="${store.id}" /></td>
			<td><c:out value="${store.name}" /></td>
			<td><c:out value="${store.logoUrl}" /></td>
			<td><c:out value="${store.logo}" /></td>
			<td><img src='<c:out value="${store.logoUrl}" /><c:out value="${store.logo}" />'/></td>
			<!-- <td><img src='http://localhost:8000/oikos/img/store/<c:out value="${store.logo}" />'/></td>-->
		</tr>
	</c:forEach>
</table>

</body>
</html>