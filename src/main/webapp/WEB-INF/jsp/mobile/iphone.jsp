<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no"/>
		<meta name="apple-mobile-web-app-capable" content="yes" />

		<title>Oikos</title>
		
		<!-- touch icon and splash image for iphone -->
		
		<!-- For iPhone 4 with high-resolution Retina display: http://mathiasbynens.be/notes/touch-icons -->
		<link rel="apple-touch-icon" sizes="114x114" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-icon-114x114.png">
		<link rel="apple-touch-startup-image" sizes="640x920" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-startup-image-640x920.png" />
		
		<!-- For first-generation iPad: -->
		<link rel="apple-touch-icon" sizes="72x72" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-icon-72x72.png">
		<link rel="apple-touch-startup-image" sizes="1004x768" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-startup-image-1004x768.png" />
		
		<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
		<link rel="apple-touch-icon" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-icon-57x57.png">
		<link rel="apple-touch-startup-image" href="<%=request.getContextPath()%>/style/img/iphone/apple-touch-startup-image-320x460.png" />		
		
		<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/style/css/styles.css" />
		<link href="https://ajax.googleapis.com/ajax/libs/dojo/1.6/dojox/mobile/themes/iphone/iphone.css" rel="stylesheet" />

		<!--[if !IE]>-->
		<link type="text/css" rel="stylesheet" media="only screen and (max-device-width: 480px)" href="<%=request.getContextPath()%>/style/css/mediaSpecificStyles.css" />
		<!--<![endif]-->
	
		<script type="text/javascript">
			djConfig = {
			  modulePaths: {
			    "dojo": "https://ajax.googleapis.com/ajax/libs/dojo/1.6/dojo",
			    "dijit": "https://ajax.googleapis.com/ajax/libs/dojo/1.6/dijit",
			    "dojox": "https://ajax.googleapis.com/ajax/libs/dojo/1.6/dojox"
			  }
			}
		</script> 	
	    <script src="https://ajax.googleapis.com/ajax/libs/dojo/1.6/dojo/dojo.xd.js" djConfig="parseOnLoad: true"> </script>
		<script language="JavaScript" type="text/javascript">
			dojo.require("dojox.mobile");	
			dojo.require("dojox.mobile.parser");
			dojo.require("dojox.mobile.TabBar");
			dojo.require("dojox.mobile.ScrollableView");
			dojo.requireIf(!dojo.isWebKit, "dojox.mobile.compat");
		</script>
		<script type="text/javascript"  type="text/javascript">
			function checkUserAgent(){	
				if (window.navigator.userAgent.indexOf('iPhone') != -1) {
					if (window.navigator.standalone == true) {
						dojo.byId("device").value="iPhone-standalone:" + window.navigator.userAgent;
					}else{
						dojo.style("ecoResponse", "opacity", "1");
						dojo.byId("ecoResponse").innerHTML = "TIPS! Legg meg til Home Screen. Bruk portrettmodus!";
						dojo.byId("device").value="iPhone-web: " + window.navigator.userAgent;
					}	
				}else if (window.navigator.userAgent.indexOf('iPad') != -1) {
					if (window.navigator.standalone == true) {
						dojo.byId("device").value="iPad-standalone:" + window.navigator.userAgent;
					}else{
						dojo.style("ecoResponse", "opacity", "1");
						dojo.byId("ecoResponse").innerHTML = "TIPS! Legg meg til Home Screen. Bruk portrettmodus!";
						dojo.byId("device").value="iPad-web: " + window.navigator.userAgent;
					}	
				}else{
					dojo.style("ecoResponse", "opacity", "1");
					dojo.byId("ecoResponse").innerHTML = "TIPS! Jeg fungerer best på en iPhone eller iPad, og liker best webkit-nettlesere som Safari eller Chrome! ";
					dojo.byId("device").value="web: " + window.navigator.userAgent;
				}
			}
		</script>
		<script type="text/javascript"  type="text/javascript">
			function checkFoodType() {
				
				var freshProduct = dojo.byId("freshProduct");
				var dryProduct = dojo.byId("dryProduct");
				var greenProduct = dojo.byId("greenProduct");
				
				dojo.connect(freshProduct, "onclick", function(event) {
			    	dojo.byId("productTypeId").value="1";
			    	dojo.byId("productType").value="fresh";
			    	dojo.byId("productTypeMessage").innerHTML="ferskmat";
			    });
				dojo.connect(dryProduct, "onclick", function(event) {
			    	dojo.byId("productTypeId").value="2";
			    	dojo.byId("productType").value="dry";
			    	dojo.byId("productTypeMessage").innerHTML="tørrmat";
				});
				dojo.connect(greenProduct, "onclick", function(event) {
			    	dojo.byId("productTypeId").value="3";
			    	dojo.byId("productType").value="green";
			    	dojo.byId("productTypeMessage").innerHTML="frukt&grønt";
				});				
			}
		</script>
		<script type="text/javascript"  type="text/javascript">
			function sendForm() {
				var form = dojo.byId("ecowishForm");
			    
				dojo.connect(form, "onsubmit", function(event) {
				    dojo.stopEvent(event);
			        dojo.style("ecoResponse", "opacity", "1");
			        var xhrArgs = {
			            form: dojo.byId("ecowishForm"),
			            url: "prodReq/add",
			            handleAs: "text",
			            load: function(data) {
			                dojo.byId("ecoResponse").innerHTML = "Tusen takk for at du gjør det enklere å velge økologisk!";
			                dojo.byId("productName").value="";
			                dojo.byId("productName").focus();
			            },
			            error: function(error) {
			                dojo.byId("ecoResponse").innerHTML = "Ønsket produkt og butikk er påkrevet!";
			            }
			        }
			        dojo.byId("ecoResponse").innerHTML = "Ønske sendes inn..."
			        var deferred = dojo.xhrPost(xhrArgs);
			    });
			}
			dojo.addOnLoad(sendForm);
			dojo.addOnLoad(checkUserAgent);	
			dojo.addOnLoad(checkFoodType);
		</script>
		
		
	</head>
	<body style="visibility:visible">
			<div dojoType="dojox.mobile.Heading">
				<div dojoType="dojox.mobile.ToolBarButton" transition="flip" moveTo="ecowishView">Økologisk ønske</div>
				<div dojoType="dojox.mobile.ToolBarButton" transition="flip" moveTo="infoView">Informasjon</div>
			</div>	
			<img class="oikosLogo" alt="Oikos logo" src="<%=request.getContextPath()%>/style/img/logo-oikos.png" />
			<div id="parentView" dojoType="dojox.mobile.ScrollableView" selected="true" keepScrollPos="false">
				<div id="ecowishView" dojoType="dojox.mobile.View" selected="true">
						<div id="ecoResponse"></div>
						<form id="ecowishForm" name="ecowishForm">
							<input id="device" name="device" type="hidden" />
							<div dojoType="dojox.mobile.RoundRectList">
								<li class="ecowishTextLarge" dojoType="dojox.mobile.ListItem">
									Jeg ville blitt glad for økologisk:
								</li>
								<li dojoType="dojox.mobile.ListItem">
									<input id="productName" name="productName" type="text" placeholder="produkt, ikke valgt" required="true">
								</li>
								<li dojoType="dojox.mobile.ListItem" class="ecowishTextSmall">
									<img id="freshProduct" src="<%=request.getContextPath()%>/style/img/foodgroup/freshnmeat_32x.png"/>
									<img id="dryProduct" src="<%=request.getContextPath()%>/style/img/foodgroup/dryfood_32x.png"/>									
									<img id="greenProduct" src="<%=request.getContextPath()%>/style/img/foodgroup/fruitngreen_32x.png"/>
									i matgruppe <span id="productTypeMessage" > ...</span>
									<input id="productTypeId" name="productTypeId" type="hidden"/>
									<input id="productType" name="productType" type="hidden" />
									på:
								</li>
								<li dojoType="dojox.mobile.ListItem">
									<select id="storeId" name="storeId">
										<option value="">butikk, ikke valgt</option>
										<c:forEach items="${stores}" var="store">
											<option value="<c:out value="${store.id}" />">
												<c:out value="${store.name}" />
											</option>
										</c:forEach>
									</select>
								</li>
								<li dojoType="dojox.mobile.ListItem">
									<select id="countyId" name="countyId">
										<option value="">fylke, ikke valgt</option>
											<c:forEach items="${counties}" var="county">
												<option value="<c:out value="${county.id}" />">
													<c:out value="${county.name}" />
												</option>
											</c:forEach>
									</select>
								</li>
								<li dojoType="dojox.mobile.ListItem">
									<input id="placeId" name="place" type="text" placeholder="sted, ikke valgt">
								</li>
								<!-- 
								<li dojoType="dojox.mobile.ListItem">
									<select id="placeId" name="placeId">
										<option value="">sted, ikke valgt</option>
											<c:forEach items="${places}" var="place">
												<option value="<c:out value="${place.id}" />">
													<c:out value="${place.name}" />
												</option>
											</c:forEach>
									</select>
								</li>
								 -->
								<li dojoType="dojox.mobile.ListItem">
									<button type="submit" dojoType="dojox.mobile.Button" id="submitButton" class="submitButton">Send inn ønske</button>
								</li>
							</div>
					</form>
					
				</div>
			
				<div id="infoView" dojoType="dojox.mobile.View" selected="false">
					<div dojoType="dojox.mobile.RoundRect" shadow="true">
						<img src="<%=request.getContextPath()%>/style/img/info-icon.png"/>
						Interessert i å kjøpe økologisk mat, men hindret av dårlig utvalg? Dette vil Oikos, med din hjelp, gjøre noe med!
						<br/><br/>Med denne app'en kan du sende inn forespørsel til butikken du er i om hvilket økologisk produkt du ønsker. 
						Skriv inn produktet, valg matgruppe, butikkkjede, fylke og sted, og trykk SEND INN ØNSKE. Forespørslene blir sendt til ansvarlig innen hver butikkkjede. 
						<br/><br/>Tusen takk for at du gjør det enklere å velge økologisk!
					</div>
				</div>		
			</div>
	</body>
</html>
