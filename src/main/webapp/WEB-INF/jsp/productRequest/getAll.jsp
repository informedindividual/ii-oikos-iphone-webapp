<%@ page import="java.text.*,java.util.*" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Statistikk</title>
</head>
<body>
<table>
	<tr>
		<td width="150">ProductName</td>
		<td width="50">ProductTypeId</td>
		<td width="150">ProductType</td>
		<td width="50">StoreId</td>
		<td width="150">Store</td>
		<td width="50">CountyId</td>
		<td width="150">County</td>
		<td width="50">PlaceId</td>
		<td width="150">Place</td>
		<td width="200">Device</td>
		<td width="150">Date</td>		
	</tr>
	<c:forEach items="${productRequests}" var="productRequest">
		<tr>
			<td><c:out value="${productRequest.productName}" /></td>
			<td><c:out value="${productRequest.productTypeId}" /></td>
			<td><c:out value="${productRequest.productType}" /></td>
			<td><c:out value="${productRequest.storeId}" /></td>
			<td><c:out value="${productRequest.store}" /></td>
			<td><c:out value="${productRequest.countyId}" /></td>
			<td><c:out value="${productRequest.county}" /></td>
			<td><c:out value="${productRequest.placeId}" /></td>
			<td><c:out value="${productRequest.place}" /></td>
			<td><c:out value="${productRequest.device}" /></td>
			<td><c:out value="${productRequest.date}" /></td>
		</tr>
	</c:forEach>
</table>

</body>
</html>