<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Product request added</title>
</head>
<body>

<h1>Product request added</h1>

<p>You have added a new product request at</p>
<%= new java.util.Date() %>
	<table>
		<tr>
			<td width="150">ProductName</td>
			<td width="150">StoreId</td>
			<td width="150">Store</td>
			<td width="150">CountyId</td>
			<td width="150">County</td>
			<td width="150">PlaceId</td>
			<td width="150">Place</td>
			
		</tr>	
		<tr>
			<td><c:out value="${productRequest.productName}" /></td>
			<td><c:out value="${productRequest.storeId}" /></td>
			<td><c:out value="${productRequest.store}" /></td>
			<td><c:out value="${productRequest.countyId}" /></td>
			<td><c:out value="${productRequest.county}" /></td>
			<td><c:out value="${productRequest.placeId}" /></td>
			<td><c:out value="${productRequest.place}" /></td>
		</tr>
	</table>

</body>
</html>